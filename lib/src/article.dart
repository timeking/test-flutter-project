class Article {
  final int id;
  final bool deleted;
  final String type;
  final String by;
  final int time;
  final String? text;
  final bool dead;
  final int? parent;
  final List<int> kids;
  final String url;
  final int score;
  final String title;
  final List<int> parts;
  final int? descendants;

  const Article({
    required this.id
    , required this.deleted
    , required this.type
    , required this.by
    , required this.time
    , required this.text
    , required this.dead
    , required this.parent
    , required this.kids
    , required this.url
    , required this.score
    , required this.title
    , required this.parts
    , required this.descendants
  });

  factory Article.fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      throw ArgumentError("Json is null");
    }
    return Article(
        id: json['id']
        , deleted: json['deleted'] ?? false
        , type: json['type']
        , by: json['by']
        , time: json['time']
        , text: json['text']
        , dead: json['dead'] ?? false
        , parent: json['parent']
        , kids: List<int>.from(json['kids'] ?? [])
        , url: json['url'] ?? ""
        , score: json['score']
        , title: json['title']
        , parts: List<int>.from(json['parts'] ?? [])
        , descendants: json['descendants'] ?? 0);
  }
}
