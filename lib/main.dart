import 'dart:async';
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:test_flutter/hn/hn_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:test_flutter/src/article.dart';
import 'package:test_flutter/hn/hackernews_api.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() {
  runApp(MyApp(bloc: HackerNewsBloc()));
}

class MyApp extends StatelessWidget {
  final HackerNewsBloc bloc;
  const MyApp({super.key, required this.bloc});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(bloc: bloc, title: 'Flutter Lecture 11'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;
  final HackerNewsBloc bloc;

  const MyHomePage({super.key, required this.title, required this.bloc});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: LoadingInfo(widget.bloc.isLoading),
      ),
      body: StreamBuilder<List<Article>>(
        stream: widget.bloc.articles,
        initialData: UnmodifiableListView<Article>([]),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data!.isNotEmpty) {
            return ListView(
              children: snapshot.data!.map(_buildWidget).toList()
            );
          } if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else {
            return const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(child: CircularProgressIndicator()));
          }
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.arrow_drop_up), label: "New Stories"),
          BottomNavigationBarItem(icon: Icon(Icons.local_fire_department), label: "Top Stories"),
        ],
        currentIndex: _selectedIndex,
        onTap: (index) {
          widget.bloc.storyType.add(index == 0 ? StoryType.newStories : StoryType.topStories);
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }

  Widget _buildWidget(Article article) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ExpansionTile(
        key: Key("${article.id}"),
        title: Text(article.title),
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Text("${article.descendants} comments"),
            MaterialButton(
                child: const Icon(Icons.launch),
                onPressed: () async {
                  final fakeUri = Uri.parse(article.url);
                  if (!await launchUrl(fakeUri)) {
                    throw 'Could not launch $fakeUri';
                  }
                }),
          ]),
        ],
      ),
    );
  }

}


class LoadingInfo extends StatefulWidget {
  final Stream<bool> _isLoading;

  const LoadingInfo(this._isLoading, {super.key});

  @override
  createState() => LoadingInfoState();
}

class LoadingInfoState extends State<LoadingInfo> with TickerProviderStateMixin {

  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: const Duration(seconds: 3));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widget._isLoading,
      builder: (context, snapshot) {
        if (!snapshot.hasData || snapshot.data!) {
        //  _controller.forward().then((value) => _controller.reverse());
           _controller.forward();
        } else {
           _controller.reverse();
        }
        return FadeTransition(
          opacity: Tween(begin: 0.0, end: 1.0).animate(_controller),
          child: const Icon(FontAwesomeIcons.squareHackerNews),
        );
      },
    );
  }
}

