import 'package:test_flutter/src/article.dart';
import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

enum StoryType {
  topStories,
  newStories
}

Future<List<int>> getStories(StoryType storyType) {
  return getStoriesJson(storyType).then((value) => parseStories(value));
}

Future<Article> getItem(int id) {
  return getItemJson(id).then((value) => parseItem(value));
}

List<int> parseStories(String jsonString) {
  var jsonDecoded = convert.jsonDecode(jsonString);
  List<int> intList = List<int>.from(jsonDecoded);
  return intList;
}

Article parseItem(String jsonString) {
  var jsonDecoded = convert.jsonDecode(jsonString);
  Article article = Article.fromJson(jsonDecoded);
  return article;
}

String baseUrl = "https://hacker-news.firebaseio.com/v0";

Future<String> getStoriesJson(StoryType storyType) async {
  var url = Uri.parse('$baseUrl/${storyType.name.toLowerCase()}.json');
  var response = await http.get(url);
  return response.body;
}

Future<String> getItemJson(int id) async {
  var url = Uri.parse("$baseUrl/item/$id.json");
  var response = await http.get(url);
  return response.body;
}