import 'dart:async';
import 'dart:collection';

import 'package:rxdart/rxdart.dart';

import 'package:test_flutter/src/article.dart';
import 'package:test_flutter/hn/hackernews_api.dart';

class HackerNewsBloc {

  final _articleSubject = BehaviorSubject<UnmodifiableListView<Article>>();
//  final _storyTypeSubject = StreamController<StoryType>();
  final _storyTypeSubject = BehaviorSubject<StoryType>.seeded(StoryType.topStories);
  final _isLoadingSubject = BehaviorSubject<bool>.seeded(false);

  Stream<List<Article>> get articles => _articleSubject.stream;
  Sink<StoryType> get storyType => _storyTypeSubject.sink;
  Stream<bool> get isLoading => _isLoadingSubject.stream;

  var _ids = <int>[];
  var _storyType = StoryType.topStories;
  var _articles = <Article>[];

  HackerNewsBloc() {
    updateStories();
    _storyTypeSubject.stream.listen((storyType) {
      _storyType = storyType;
      updateStories();
    });
  }

  void updateStories() async {
    _isLoadingSubject.add(true);
    _articleSubject.add(UnmodifiableListView([]));
    await _updateIds();
    await _updateArticles();
    _articleSubject.add(UnmodifiableListView(_articles));
    _isLoadingSubject.add(false);
  }

  Future<Null> _updateIds() async {
    final ids = await getStories(_storyType);
    _ids = ids.sublist(0, 20);
  }

  Future<Null> _updateArticles() async {
    final articleFutures = _ids.map((id) => _getItemSafe(id));
    final articles = await Future.wait(articleFutures);
    _articles = articles;
  }

  Future<Article> _getItemSafe(int id) async {
    try {
      return getItem(id);
    } catch(e) {
      print(e);
      return Article(id: id, deleted: false, type: 'Article', by: 'Unknown', time: 0,
          text: 'Dummy text of article $id', dead: false, parent: null, kids: [],
          url: "", score: 0, title: 'Cannot download $id', parts: [], descendants: 0);
    }
  }

}